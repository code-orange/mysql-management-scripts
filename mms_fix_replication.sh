#!/bin/bash

set -e

if [ "$#" -ne 1 ]
then
	echo "Incorrect number of arguments"
	echo "Did you add the config file as parameter?"
	exit 1
fi

# DEFAULT VARS
SLAVE_MODE=0
ALL_DATABASES=1
DATABASES="information_schema"

INCLUDE_TRIGGERS=1
INCLUDE_ROUTINES=1

MYSQL_SRC_HOST=localhost
MYSQL_SRC_PORT=3306
MYSQL_SRC_USER=root
MYSQL_SRC_PASS=

MYSQL_DST_HOST=localhost
MYSQL_DST_PORT=3306
MYSQL_DST_USER=root
MYSQL_DST_PASS=

# LOAD CONFIG
. $1

# BACKUP SOURCE
true > /root/mysql_data.sql

if [ $SLAVE_MODE -eq 1 ] && [ $ALL_DATABASES -eq 0 ]
then
	echo "You can not recover a slave without cloning all databases"
	exit 1
fi

if [ $SLAVE_MODE -eq 1 ]
then
	echo "STOP SLAVE;" >> /root/mysql_data.sql
fi

MYSQLDUMP_OPTIONS="-h${MYSQL_SRC_HOST} -P${MYSQL_SRC_PORT} -u${MYSQL_SRC_USER} -p${MYSQL_SRC_PASS} --single-transaction"

if [ $ALL_DATABASES -eq 1 ]
then
	MYSQLDUMP_OPTIONS="${MYSQLDUMP_OPTIONS} --all-databases"
else
	MYSQLDUMP_OPTIONS="${MYSQLDUMP_OPTIONS} --databases ${DATABASES}"
fi

if [ $INCLUDE_TRIGGERS -eq 1 ]
then
	MYSQLDUMP_OPTIONS="${MYSQLDUMP_OPTIONS} --triggers"
fi

if [ $INCLUDE_ROUTINES -eq 1 ]
then
	MYSQLDUMP_OPTIONS="${MYSQLDUMP_OPTIONS} --routines"
fi

if [ $SLAVE_MODE -eq 1 ]
then
	MYSQLDUMP_OPTIONS="${MYSQLDUMP_OPTIONS} --master-data=1"
fi

mysqldump ${MYSQLDUMP_OPTIONS} >> /root/mysql_data.sql

if [ $SLAVE_MODE -eq 1 ]
then
	echo "START SLAVE;" >> /root/mysql_data.sql
fi

# WRITE DESTINATION
mysql -h${MYSQL_DST_HOST} -P${MYSQL_DST_PORT} -u${MYSQL_DST_USER} -p${MYSQL_DST_PASS} < /root/mysql_data.sql

rm -f /root/mysql_data.sql
