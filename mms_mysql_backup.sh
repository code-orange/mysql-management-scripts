#!/bin/bash

OUTPUTDIR="/backup/mysql"

mkdir -p "$OUTPUTDIR"
rm "$OUTPUTDIR/*gz" > /dev/null 2>&1

databases=`mysql -e "SHOW DATABASES;" | tr -d "| " | grep -v Database`

datecode=`date +%Y%m%d`

for db in $databases; do
    echo "Dumping database: $db"
    mysqldump --opt --single-transaction --compress --hex-blob --quick --max_allowed_packet=512M --skip-lock-tables --default-character-set=utf8mb4 --databases $db | gzip > "${OUTPUTDIR}/${datecode}_${db}.sql.gz"
done
